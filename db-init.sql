USE [master];
GO
  
DECLARE @distributor AS SYSNAME;
DECLARE @distributorlogin AS SYSNAME;
DECLARE @distributorpassword AS SYSNAME;
DECLARE @Server SYSNAME;
  
SELECT @Server = @@servername;
  
SET @distributor = @Server;
SET @distributorlogin = N'UserDistributor';
SET @distributorpassword = N'Use4Distributor';
  
EXEC sp_adddistributor @distributor = @distributor;
  
EXEC sp_adddistributiondb @database = N'distribution'
    ,@log_file_size = 2
    ,@deletebatchsize_xact = 5000
    ,@deletebatchsize_cmd = 2000
    ,@security_mode = 0
    ,@login = @distributorlogin
    ,@password = @distributorpassword;
GO

-- let's check the DB
USE distribution;
GO

-- see the repl commands table
SELECT * 
FROM [dbo].[MSrepl_commands]

-- and let's see the jobs we made
SELECT name, date_modified 
FROM msdb.dbo.sysjobs 
ORDER by date_modified desc