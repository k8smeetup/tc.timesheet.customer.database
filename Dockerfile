#Doesn't work with hadr
#unable to execute command as sudo
#FROM mcr.microsoft.com/mssql/server:2019-GA-ubuntu-16.04 as builder

FROM microsoft/mssql-server-linux:latest

COPY . /

ENV ACCEPT_EULA=Y SA_PASSWORD=Use4SqlServer2020*01 MSSQL_PID=Enterprise MSSQL_AGENT_ENABLED='true'

ENV CERTFILE "certificate/dbm_certificate.cer"
ENV CERTFILE_PWD "certificate/dbm_certificate.pvk"

RUN mkdir /usr/certificate
WORKDIR /usr/   
COPY ${CERTFILE} ./certificate
COPY ${CERTFILE_PWD} ./certificate

# Set permissions (if you are using docker with windows, you don´t need to do this)
#RUN chown mssql:mssql /usr/certificate/dbm_certificate.pvk
#RUN chown mssql:mssql /usr/certificate/dbm_certificate.cer

RUN /opt/mssql/bin/mssql-conf set hadr.hadrenabled 1
RUN /opt/mssql/bin/mssql-conf set sqlagent.enabled true

EXPOSE 1433
EXPOSE 5022

CMD /opt/mssql/bin/sqlservr

RUN /bin/bash /db-init.sh